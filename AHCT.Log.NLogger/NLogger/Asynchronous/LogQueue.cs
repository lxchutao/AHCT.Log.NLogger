/******************************************
 * 
 * 模块名称：内部类 LogQueue
 * 当前版本：1.0
 * 开发人员：楚涛
 * 开发时间：2011-06-28
 * 版本历史：
 * 
 * Copyright (C) lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ群:60873348  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 * 
 ******************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace AHCT.Log.Asynchronous
{
    internal class LogQueue
    {
        /// <summary>
        /// 日志 队列
        /// </summary>
        //public static Queue<LogEntity> LOG_QUEUE = new Queue<LogEntity>();
        internal static Queue LOG_QUEUE = Queue.Synchronized(new Queue(512));
    }
}

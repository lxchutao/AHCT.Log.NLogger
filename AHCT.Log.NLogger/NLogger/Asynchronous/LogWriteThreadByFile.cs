/******************************************
 * 
 * 模块名称：内部类 LogWriteThreadByFile
 * 当前版本：1.0
 * 开发人员：楚涛
 * 开发时间：2011-06-29
 * 版本历史：
 * 
 * Copyright (C) lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ群:60873348  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 * 
 ******************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Threading;
using System.IO;

namespace AHCT.Log.Asynchronous
{
    internal class LogWriteThreadByFile
    {
        private string _filename_full;
        private Queue _queue_contents;
        private Thread _thread;
        private int _thread_wait = 100;

        internal LogWriteThreadByFile(string filename_full)
        {
            this._filename_full = filename_full;
            this._queue_contents = Queue.Synchronized(new Queue(512));
            this._thread = new Thread(WriteLogContents);
            this._thread.IsBackground = NLogger.GetInstance().THREAD_IS_BACKGROUD;
            this._thread.Start();
            if (NLogger.GetInstance().LOG_THREAD_WAIT > 0)
            {
                this._thread_wait = NLogger.GetInstance().LOG_THREAD_WAIT;
            }
        }

        /// <summary>
        /// 日志文件全名 路径+文件名+文件扩展名
        /// </summary>
        internal string FILENAME_FULL
        {
            get { return this._filename_full; }
        }
        /// <summary>
        /// 日志内容
        /// </summary>
        internal Queue QUEUE_CONTENTS
        {
            get { return this._queue_contents; }
        }

        /// <summary>
        /// 对同一个日志文件循环追加日志内容，当日志队列为空时线程Sleep一段时间
        /// </summary>
        private void WriteLogContents()
        {
            while (true)
            {
                while (this.QUEUE_CONTENTS.Count > 0)
                {
                    LogEntity entity = this.QUEUE_CONTENTS.Dequeue() as LogEntity;
                    if (entity != null)
                    {
                        FileInfo fi = new FileInfo(this.FILENAME_FULL);
                        if (fi.Exists)
                        {
                            //1M = 1*1024*1024=1048576
                            if (fi.Length > NLogger.GetInstance().LogFileMaxSize)
                            {
                                //fi.MoveTo(entity.FILE_DIR_NAME + entity.LOG_FILE_SUFFIX + "_" + DateTime.Now.Ticks.ToString() + entity.LOG_EXTENSION);
                                string newFileName = entity.FILE_DIR_NAME + entity.LOG_FILE_SUFFIX + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + entity.LOG_EXTENSION;
                                //fi.MoveTo(newFileName);
                                LogUtil.FileRename(fi, newFileName);
                            }
                        }
                        try
                        {
                            File.AppendAllText(this.FILENAME_FULL, entity.LOG_CONTENTS + "\r\n", Encoding.UTF8);
                        }
                        catch 
                        {
                            System.Threading.Thread.Sleep(100);
                            this.QUEUE_CONTENTS.Enqueue(entity);
                        }
                    }
                }
                if (this.QUEUE_CONTENTS.Count <= 0)
                {
                    if (this._thread_wait > 0)
                    {
                        Thread.Sleep(this._thread_wait);
                    }
                }
            }
        }
    }
}

/******************************************
 * 
 * 模块名称：内部类 LogThread
 * 当前版本：1.0
 * 开发人员：楚涛
 * 开发时间：2011-06-28
 * 版本历史：
 * 
 * Copyright (C) lxchutao(楚涛) 2011-2015  lxchutao@163.com  QQ群:60873348  QQ:290363711  http://blog.csdn.net/chutao  http://lxchutao.cnblogs.com
 * 
 ******************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace AHCT.Log.Asynchronous
{
    internal class LogThread
    {
        /// <summary>
        /// 启动日志异步写入线程
        /// </summary>
        /// <param name="wait">当日志队列为空时等待的毫秒数</param>
        /// <param name="threadIsBackgroud">true后台线程，false前台线程</param>
        internal static void RunAsynchronous(int wait, bool threadIsBackgroud)
        {
            LogWrite log = new LogWrite();
            LogWrite.isRun = true;
            if (wait >= 0)
            {
                LogWrite.threadWait = wait;
            }
            Thread logThread = new Thread(log.LogAppend);
            //logThread.IsBackground = true;//true 设置为后台线程，既前台线程全部结束后，后台也跟着结束。
            logThread.IsBackground = threadIsBackgroud;//false 前台线程
            logThread.Start();
        }
    }
}

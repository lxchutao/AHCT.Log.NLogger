﻿using System;
using System.Collections.Generic;
using System.Text;
using AHCT.Log;

namespace AHCT.DemoLog
{
    internal class LogDemo
    {
        public static void Test1()
        {
            NLogger.GetInstance().Debug("DemoTest打印Debug日志。");
            NLogger.GetInstance().Info("DemoTest打印Info日志。");
            NLogger.GetInstance().Warn("DemoTest打印Warn日志。");
            NLogger.GetInstance().Error("DemoTest打印Error日志。");
            NLogger.GetInstance().Fatal("DemoTest打印Fatal日志。");
        }

        public static void Test2()
        {
            NLogger logger = NLogger.GetInstance();
            logger.Debug("测试环境打印出的Debug日志");
            logger.Info("测试环境打印出的Info日志");
            logger.Warn("测试环境打印出的Warn日志");
            logger.Error("测试环境打印出的Error日志");
            logger.Fatal("测试环境打印出的Fatal日志");
        }
    }
}

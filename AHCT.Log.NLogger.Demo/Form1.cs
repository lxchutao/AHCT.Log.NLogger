using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AHCT.DemoLog;

namespace AHCT.Log.Demo
{
    public partial class Form1 : Form
    {
        NLogger logger = NLogger.GetInstance();

        public Form1()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            logger.Debug("测试环境打印出的Debug日志");
            logger.Info("测试环境打印出的Info日志");
            logger.Warn("测试环境打印出的Warn日志");
            logger.Error("测试环境打印出的Error日志");
            logger.Fatal("测试环境打印出的Fatal日志");

            LogDemo.Test1();
            LogDemo.Test2();
        }

    }
}